/* 
 * File:   main.cpp
 * Author: HMK
 *
 * Created on 18 Temmuz 2013 Perşembe, 17:42
 */

#include <iostream>
#include "include/CLinkedList.h"

using namespace std;

int main()
{
    CLinkedList<int> liste;
    liste.push_back(10);
    liste.push_back(20);
    liste.push_back(30);
    liste.push_back(40);
    liste.add(3, 25);
    cout << "List size: " << liste.size() << endl;
    cout << "List\'s member: \n";
    liste.display();
    
    cout << "Index of 20: " <<liste.find(20) << endl;
    cout << "List\'s member after removing: \n";
    liste.remove(2);  // 10, 20, 25, 30, 40 -> remove 20
    liste.remove(2);  // 10, 25, 30, 40     -> remove 25
    liste.pop_back(); // 10, 30, 40         -> remove 40
    liste.display();  // 10, 30
    return 0;
}

