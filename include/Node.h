/* 
 * File:   Node.h
 * Author: hmert
 *
 * Created on 18 Temmuz 2013 Perşembe, 17:45
 */

#ifndef CNODE_H
#define	CNODE_H

template<typename T> struct CNode
{
    CNode<T>() = default;
    
    T mData;
    CNode<T> *mNextNode;
    CNode<T> *mPrevNode; // for doubly linked list 
    CNode<T> &operator=(CNode<T> &rvalue)
    {
        mData = rvalue.mData;
        mNextNode = rvalue.mNextNode;
        mPrevNode = rvalue.mPrevNode;
        return *this;
    }
};

#endif	/* CNODE_H */

