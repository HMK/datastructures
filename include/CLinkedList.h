/* 
 * File:   CLinkedList.h
 * Author: hmert
 *
 * Created on 18 Temmuz 2013 Perşembe, 17:47
 */

#ifndef CLINKEDLIST_H
#define	CLINKEDLIST_H

#include "Node.h"
#include <iostream>

template <typename T> 
class CLinkedList {
public:
    CLinkedList() : mFirst(nullptr), mSize(0) {}
    CLinkedList(const CLinkedList& orig);
    virtual ~CLinkedList() {}
    
    bool push_back(T data);
    bool add(size_t index, T data);
    size_t find(T data);
    size_t size() const { return mSize; }
    bool pop_back(); // Remove last node
    bool remove(size_t index); // Remove indexth node
    void display() const;
private:
    CNode<T> *mFirst;
    size_t mSize;
};


template<typename T> bool CLinkedList<T>::push_back(T data)
{    
    CNode<T> *newNode = new CNode<T>();
    
    newNode->mData = data;
    newNode->mNextNode = nullptr;
    
    // add data to list if list is empty
    if(mFirst == nullptr)
    {    
        mFirst = newNode;
        mSize++;
        return true;
    }
    else
    {
        CNode<T> *tempNode = mFirst;
        // iterate last node
        while(mFirst->mNextNode)
            mFirst = mFirst->mNextNode;
        
        mFirst->mNextNode = newNode;
        mFirst = tempNode;
        mSize++;
        return true;
    }
}

template<typename T> 
bool CLinkedList<T>::add(size_t index, T data)
{
    if(index > mSize)
        return false;
    
    CNode<T> *newNode = new CNode<T>();
    newNode->mData = data;
    
    CNode<T> *currNode = mFirst;
    CNode<T> *prevNode = nullptr;
    
    if(mSize == index)
    {
        while(currNode->mNextNode)
        {
            currNode = currNode->mNextNode;
        }
        currNode->mNextNode = newNode;
        mSize++;
        return true;
    }
    
    currNode = mFirst;
    prevNode = nullptr;
    
    size_t currIndex = 1;
    while(currIndex < index)
    {
        prevNode = currNode;
        currNode = currNode->mNextNode;
        currIndex++;
    }
    
    if(prevNode)
    {
        prevNode->mNextNode = newNode;
        newNode->mNextNode = currNode;
        mSize++;
        return true;
    }
    else
    {
        newNode->mNextNode = currNode;
        mFirst = newNode;
        mSize++;
        return true;
    }
    
    return true;
}

template<typename T> 
void CLinkedList<T>::display() const
{
    CNode<T> *currNode = mFirst;
    while(currNode)
    {
        std::cout << currNode->mData << std::endl;
        currNode = currNode->mNextNode;
    }
}

template<typename T> 
size_t CLinkedList<T>::find(T data)
{
    CNode<T> *currNode = mFirst;
    size_t index = 1;
    while(currNode)
    {
        if(data == currNode->mData)
            return index;
        index++;
        currNode = currNode->mNextNode;
    }
    return 0;
}

template<typename T> 
bool CLinkedList<T>::remove(size_t index)
{
    index--;
    if(index >= mSize)
        return false;
    
    CNode<T> *currNode = mFirst;
    CNode<T> *prevNode = nullptr;
    
    for(size_t i = 0; i < index; i++)
    {
        prevNode = currNode;
        currNode = currNode->mNextNode;
    }
    
    // remove first node
    if(!prevNode)
    {
        mFirst = currNode->mNextNode;
        currNode->mNextNode = nullptr;
        mSize--;
        return true;
    }
    else
    {
        prevNode->mNextNode = currNode->mNextNode;
        currNode->mNextNode = nullptr;
        mSize--;
        return true;
    }
}

template<typename T> 
bool CLinkedList<T>::pop_back()
{
    CNode<T> *currNode = mFirst;
    CNode<T> *prevNode = nullptr;
    while(currNode->mNextNode)
    {
        prevNode = currNode;
        currNode = currNode->mNextNode;
    }
    if(prevNode)
    {
        prevNode->mNextNode = nullptr;
    }
    return true;
}
#endif	/* CLINKEDLIST_H */
